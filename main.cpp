#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int KnapSack(int cap,vector<vector<int>> & points)
{


    if(cap == 0 || points.size()==0)
    {
        return 0;
    }

    int maxvalue =0;

    vector <vector<double>> profits;

    for (uint i=0; i<points.size();i++)
    {
        double profit = static_cast<double>(points[i][1])/static_cast<double>(points[i][0]);
        vector <double> tempvec;
        tempvec.push_back(profit);
        tempvec.push_back(i);
        profits.push_back(tempvec);
    }


    for (uint i = 0; i<profits.size(); i++)
    {
        for (uint j=1; j<profits.size()-i;j++)
        {
            if (profits[j][0]<profits[j-1][0])
            {
                swap(profits[j],profits[j-1]);
            }
        }
    }




    int actualwage = 0;
    for (uint i = profits.size()-1;i>0;i--)
    {

      int loc = static_cast<int>(profits[i][1]);
      if(actualwage+points[loc][0]>=cap)
      {
          continue;
      }
      int ammout =1; //(cap-actualwage)/points[loc][0];
      maxvalue+=ammout*points[loc][1];
      actualwage+=ammout * points[loc][0];

    }

    return maxvalue;

}

int main()
{
int capacity;
int numberoftestcases;
int numofartefacts;
freopen("dane.txt","r",stdin);
cin>> numberoftestcases;

for (int i = 0; i<numberoftestcases;i++)
{
    cin >> capacity;
    cin >> numofartefacts;
    vector<vector<int>> artefacts;
    for (int i = 0; i< numofartefacts;i++)
    {

        int temp;
        cin >> temp;
        vector <int> tempvect;
        tempvect.push_back(temp);
        cin >> temp;
        tempvect.push_back(temp);

        artefacts.push_back(tempvect);


    }
    cout << KnapSack(capacity, artefacts) << '\n';
}


    return 0;
}
